## Introduction
`checkbaudrate.py` is a simple **python 2.7** script to check baud rate for serial connection. The script has been tested on,
- Mac OS
- Ubuntu Linux

## Pre-requisites
Before running the script:
- Make sure you have pyserial installed. (pip install pyserial)
- power up your device, wait for a few seconds for it boot.
- connect to your serial device.

## How does it work?
1. The script will first list all serial connection on your machine.
2. It will prompt user to select connection by numeric ID
3. It will attempt to connect with the following baud rates: "4800","9600","38400","19200","57600","115200"
4. It will send a new line + carriage return and print out the response
5. The correct buad rate is the that is not printing gibberish.

## Example
In this example the correct baud rate is 115200
```
$ sudo python checkbaudrate.py
[0] /dev/cu.Bluetooth-Incoming-Port
[1] /dev/cu.usbserial-1430
Select device number: 1
/dev/cu.usbserial-1430
-------------------
Baudrate: 4800
Output: ?
-------------------
Baudrate: 9600
Output: ?
-------------------
Baudrate: 38400
?*|?;8?0???і??4?4194??ZYz??hɞ??xM?"xC?*?:xp۱
0???
-------------------
Baudrate: 19200
Output: M??9??U._?R?p?)pD??$???L?
-------------------
Baudrate: 57600
Output: ?%???N


'aH???
      kC???C??ŧ?ƆƤ????
w??
?{w?0??o1!χ1??
-------------------
Baudrate: 115200
Output:


-------------------
Program Exit

```
