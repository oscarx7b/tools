import serial # pre-requsite: pip install pyserial
import serial.tools.list_ports

baudRates = ["4800","9600","38400","19200","57600","115200"]
devs = serial.tools.list_ports.comports()
for dev in devs:
	print ('[' + str(devs.index(dev)) + '] ' + dev.device)

try:
	devIndex = int(input('Select device number: '))
	if (devIndex < 0) or (devIndex > len(devs)-1):
		print('Selection out of range.')
	else:
		port = devs[devIndex].device
		for i in baudRates:
			ser = serial.Serial(port, i, timeout=1)
			ser.write("\n\r")
			s = ser.read(50) #print first 50 bytes
			print ('-------------------')
			print ('Baudrate: ' + i)
			print ('Output: ' + s)
			ser.close()
		print ('-------------------')
except NameError:
	print('Selection is not valid.')
print('Program Exit\n')
